﻿

#include <iostream>
#include <cmath>

class Vector
{
public:

    Vector()
    {
        x = 0;
        y = 0;
        z = 0;
    }

    Vector(float x, float y, float z)
    {
        this->x = x;
        this->y = y;
        this->z = z;
    }

    operator float()
    {
        return sqrt(x * x + y * y + z * z);
    }

    friend Vector operator+(const Vector& a, const Vector& b);

    // Умножение
    friend Vector operator*(const Vector& a, const float& b); 

    // Разность
    friend Vector operator-(const Vector& a, const Vector& b);

    friend std::ostream& operator<<(std::ostream& out, const Vector& v);

    // Ввод
    friend std::ifstream& operator>>(std::istream& in, const float& v);

    friend bool operator>(const Vector& a, const Vector& b);

    float operator[](int index)
    {
        switch (index)
        {
        case 0:
            return x;
            break;
        case 1:
            return y;
            break;
        case 2:
            return z;
            break;
        default:
            std::cout << "Index error";
            return 0;
            break;
        }
    }

private:

    float x;
    float y;
    float z;
};

bool operator>(const Vector& a, const Vector& b)
{
    return false;
}

Vector operator+(const Vector& a, const Vector& b)
{
    return Vector(a.x + b.x, a.y + b.y, a.z + b.z);
}

// Умножение
Vector operator*(const Vector& a, const float& b)
{
    return Vector(a.x * b, a.y * b, a.z * b);
}

// Разность
Vector operator-(const Vector& a, const Vector& b)
{
    return Vector(a.x - b.x, a.y - b.y, a.z - b.z);
}

std::ostream& operator<<(std::ostream& out, const Vector& v)
{
    out << " " << v.x << " " << v.y << " " << v.z;
    return out;
}

int main()
{
    Vector v1(0, 1, 2);
    Vector v2(3, 4, 5);
    Vector v3;

    float input1 = 0;
    float input2 = 0;
    float input3 = 0;

    while (true)
    {
        std::cout << "Input vector numb 1: ";

        if((std::cin >> input1))
        {
            std::cout << "Input vector numb 2: ";

            if ((std::cin >> input2))
            {
                std::cout << "Input vector numb 3: ";

                if ((std::cin >> input3))
                {
                    break;
                }
                else
                {
                    std::cout << "Incorrect input3." << '\n';
                    return 1;
                }
            }
            else
            {
                std::cout << "Incorrect input2." << '\n';
                return 1;
            }
        }
        else
        {
            std::cout << "Incorrect input1." << '\n';
            return 1;
        }
    }

    Vector v4(input1, input2, input3);

    std::cout << "v1 : " << v1 << '\n';

    std::cout << "InputVector : " << v4 << '\n';

    //v3 = v1 + v2;
    //std::cout << v3 << '\n';
    //std::cout << "v3 length " << static_cast<float>(v3) << '\n';

    // Умножение
    std::cout << "InputVector Multiplication * 9 : " << v4 * 9.0f << '\n';

    // Разность
    std::cout << "InputVector Difference - v1 :" << v4 - v1;
}
